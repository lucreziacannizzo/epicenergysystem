package it.lmc.energy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EpicEnergySystemAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(EpicEnergySystemAppApplication.class, args);
	}

}
