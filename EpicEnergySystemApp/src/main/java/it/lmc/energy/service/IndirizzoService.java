package it.lmc.energy.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.lmc.energy.model.Comune;
import it.lmc.energy.model.Indirizzo;
import it.lmc.energy.model.utility.IndirizzoUtility;

public interface IndirizzoService {
	public Indirizzo save(Indirizzo indirizzo);
	public Comune trovaComune(String comune);
	public Indirizzo save(IndirizzoUtility indirizzo);
	public Page<Indirizzo> findAllByComune( Comune comune, Pageable pageable);
}
