package it.lmc.energy.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.lmc.energy.model.Contatto;
import it.lmc.energy.model.Fattura;
import it.lmc.energy.model.Utente;
import it.lmc.energy.model.utility.ContattoUtility;
import it.lmc.energy.model.utility.FatturatoAnnualeUtility;
import it.lmc.energy.model.utility.IndirizzoUtility;
import it.lmc.energy.model.utility.UtenteUtility;

public interface UtenteService {
	public BigDecimal contaImportoTotale(List<Fattura> fatture); // Inserire discriminante per anno

	public Page<Fattura> findFatturaByIdUtente(Long id, Pageable pageable);

	public Page<Utente> findAllSorted(Integer page, Integer size, String sort);

	public Optional<Utente> findById(Long id);

	public Utente save(UtenteUtility utente, String tipo, ContattoUtility contatto, IndirizzoUtility indirizzoLegale,
			IndirizzoUtility indirizzoOperativo);

	public Contatto updateContatto(Long id, ContattoUtility contatto);

	public String delete(Long id);

	public String delete(String email);

	public Optional<Utente> findByEmail(String email);

	public Optional<Utente> findByPIva(String pIva);

	public Page<Utente> findByRagioneSociale(String ragioneSociale, Pageable pageable);

	public Page<Utente> findByDataInserimento(int d, int m, int y, Pageable pageable);

	public Page<Utente> findByDataUltimoContatto(int d, int m, int y, Pageable pageable);

	public Optional<Utente> findByPec(String pec);

	public Optional<Utente> findByTelefono(String telefono);

	public Optional<Utente> findByContattoEmail(String nome);

	public Optional<Utente> findByContattoNome(String nome, String Cognome);

	public Optional<Utente> updateDataUltimoContatto(Long id);

	public BigDecimal getFatturatoAnnualeByUtente(Long id, int anno);

	public Page<FatturatoAnnualeUtility> findAllSortedByFatturatoAnnuale(int anno, Pageable pageable);

	public Page<FatturatoAnnualeUtility> findByFatturatoMinore(BigDecimal cifra, int anno, Pageable pageable);

	public Page<FatturatoAnnualeUtility> findByFatturatoMaggiore(BigDecimal cifra, int anno, Pageable pageable);

	public Optional<Utente> findByContattoTelefono (String telefono);
	public Page<Utente> findByParteNome(String nome, Pageable pageable);
	
	public Page<Utente> findAllByTipoAzienda(String nome, Pageable pageable);
	
	public Optional<Utente> updateFatturatoAnnuale(Long id, BigDecimal importo);
	
}
