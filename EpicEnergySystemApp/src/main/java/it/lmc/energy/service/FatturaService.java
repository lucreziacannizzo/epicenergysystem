package it.lmc.energy.service;

import java.math.BigDecimal;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.lmc.energy.model.Fattura;
import it.lmc.energy.model.utility.FatturaUtility;
import it.lmc.energy.model.utility.FatturatoAnnualeUtility;


public interface FatturaService {
	public Page<Fattura> findFatturaByUtente(Long utente, Pageable pageable);
	public Page<Fattura> findByStato(String stato, Pageable pageable);
	public Page<Fattura> findByData(int day, int month, int year, Pageable pageable);
	public Page<Fattura> findByAnno(int anno, Pageable pageable);
	public Page<Fattura> findByRange(BigDecimal min, BigDecimal max, Pageable pageable);
	public Fattura save (Long idUtente, BigDecimal importo);
	public String delete(Long idFattura);
	public String delete(Long idUtente, int numeroFattura);
	public String updateStato(Long idFattura, String stato);
	public Fattura update(Long idFattura, FatturaUtility fattura);
	
	
}
