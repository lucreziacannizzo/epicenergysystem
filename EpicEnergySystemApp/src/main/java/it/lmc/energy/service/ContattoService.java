package it.lmc.energy.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.lmc.energy.model.Contatto;
import it.lmc.energy.model.utility.ContattoUtility;

public interface ContattoService {
	public Optional<Contatto> findByEmail(String Email);
	public Optional<Contatto> findByTelefono(String telefono);
	public Page<Contatto> findAll(Pageable pageable);
	public Page<Contatto> findByNomeECognome(String nome, String cognome, Pageable pageable);
	public Contatto save(ContattoUtility contatto);
	public String delete(Long id);
	public Contatto save(Contatto contatto);
	public Optional<Contatto> findById(Long id);
}
