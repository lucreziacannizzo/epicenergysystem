package it.lmc.energy.service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import it.lmc.energy.model.Comune;
/*
 * Interfaccia di servizio della classe Comune
 */
public interface ComuneService {
	public void save (Comune comune);
	public String popola () throws IOException;
	public Optional<Comune> findByNome (String nome);
}
