package it.lmc.energy.service;

import java.util.Optional;

import it.lmc.energy.model.Provincia;

public interface ProvinciaService {
	public Provincia save(Provincia provincia);
	Optional<Provincia> findBySigla(String sigla);
}
