package it.lmc.energy.repository;



import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import it.lmc.energy.model.Contatto;
import it.lmc.energy.model.Fattura;
import it.lmc.energy.model.Indirizzo;
import it.lmc.energy.model.TipoAzienda;
import it.lmc.energy.model.Utente;

public interface UtenteRepository extends JpaRepository<Utente, Long> {
	
	@Query("Select u from Utente u Order by u.sedeLegale.comune.provincia")
	public Page<Utente> findAllByProvincia(Pageable pageable);
	@Query("Select u from Utente u where u.ragioneSociale like '%:s%' ")
	public Page<Utente> findByParteNome(String s, Pageable pageable);
	public Optional<Utente> findByEmail(String email);
	public Optional<Utente> findByIva(String pIva);
	public Page<Utente> findAllByRagioneSociale( String ragioneSociale, Pageable pageable);
	public Page<Utente> findAllByDataInserimento(Date dataInserimento,Pageable pageable);
	public Page<Utente> findAllByDataUltimoContatto (Date dataUltimoContatto, Pageable pageable);
	public Optional<Utente> findByPec(String pec);
	public Optional<Utente> findByTelefono(String telefono);
	public Optional<Utente> findByContatto(Contatto contatto);
	public Page<Utente> findAll(Pageable pageable);
	public Page <Utente> findAllByTipo(TipoAzienda tipo, Pageable pageable);

}
