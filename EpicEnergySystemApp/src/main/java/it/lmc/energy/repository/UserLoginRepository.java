package it.lmc.energy.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import it.lmc.energy.model.UserLogin;

public interface UserLoginRepository extends JpaRepository<UserLogin, Long> {
	Optional<UserLogin> findByUsername(String nome);
}
