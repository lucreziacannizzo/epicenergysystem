package it.lmc.energy.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import it.lmc.energy.model.Contatto;

public interface ContattoRepository extends JpaRepository<Contatto, Long> {
	public Optional<Contatto> findByEmail(String email);
	public Page<Contatto> findAllByNomeAndCognome(String nome, String cognome, Pageable pageable);
	public Page<Contatto> findAll(Pageable pageable);
	public Optional<Contatto> findByTelefono(String telefono);
	
}
