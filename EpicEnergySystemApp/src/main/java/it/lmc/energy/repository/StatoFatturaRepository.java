package it.lmc.energy.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import it.lmc.energy.model.StatoFattura;

public interface StatoFatturaRepository extends JpaRepository<StatoFattura, Long>{
	public Optional<StatoFattura> findByStato(String stato);
}
