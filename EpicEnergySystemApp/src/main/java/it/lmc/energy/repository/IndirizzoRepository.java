package it.lmc.energy.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.lmc.energy.model.Comune;
import it.lmc.energy.model.Indirizzo;

@Repository
public interface IndirizzoRepository extends JpaRepository<Indirizzo, Long>{
	public Page<Indirizzo> findAllByComune(Comune comune, Pageable pageable);
}
