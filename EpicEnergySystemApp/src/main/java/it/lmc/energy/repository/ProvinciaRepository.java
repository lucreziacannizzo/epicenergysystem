package it.lmc.energy.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.lmc.energy.model.Provincia;
@Repository
public interface ProvinciaRepository extends JpaRepository<Provincia, Long> {
	Optional<Provincia> findBySigla(String sigla);
}
