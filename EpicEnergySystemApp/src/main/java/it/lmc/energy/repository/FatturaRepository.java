package it.lmc.energy.repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import it.lmc.energy.model.Fattura;
import it.lmc.energy.model.StatoFattura;
import it.lmc.energy.model.Utente;
import it.lmc.energy.model.utility.FatturatoAnnualeUtility;

public interface FatturaRepository extends JpaRepository<Fattura, Long> {
	@Query("Select sum (f.importo) from Fattura f where YEAR(f.data)=:anno and f.utente.id=:id")
	public BigDecimal fatturatoAnnuale(int anno, Long id);

	// Riga sql: select email, sum(f.importo) as importo from utente u, fattura f
	// where f.utente_id=u.id and f.anno=2022 group by u.id order by importo
	@Query(value = "Select (select u.email from Utente u where u.id= f.utente.id) as email, sum(f.importo) as importo from Fattura f where year(f.data) = :anno Group by f.utente.id order by importo", countQuery = "Select count(*) from Fattura f where year(f.data) = :anno Group by f.utente.id")
	public Page<FatturatoAnnualeUtility> findAllSortedByFatturato(int anno, Pageable pageable);

	@Query(value = "Select (select u.email from Utente u where u.id= f.utente.id) as email, sum(f.importo) as importo from Fattura f where year(f.data) = :anno Group by f.utente.id having sum(f.importo)<:cifra order by importo", 
			countQuery = "Select count(*) from Fattura f where year(f.data) = :anno Group by f.utente.id")
	public Page<FatturatoAnnualeUtility> findByFatturatoMinore(BigDecimal cifra, int anno, Pageable pageable);

	@Query(value = "Select (select u.email from Utente u where u.id= f.utente.id) as email, sum(f.importo) as importo from Fattura f where year(f.data) = :anno Group by f.utente.id having sum(f.importo)>:cifra order by importo", 
			countQuery = "Select count(*) from Fattura f where year(f.data) = :anno Group by f.utente.id")
	public Page<FatturatoAnnualeUtility> findByFatturatoMaggiore(BigDecimal cifra, int anno, Pageable pageable);
	
	@Query("Select sum(f.importo) from Fattura f where f.utente.id=:id and year(f.data)=:anno")
	public BigDecimal fatturatoAnnualeByIdUtente(int anno, Long id);

	@Query("Select f from Fattura f where f.stato= :stato")
	public Page<Fattura> findAllByStato(StatoFattura stato, Pageable pageable);

	public Page<Fattura> findAllByData(Date data, Pageable pageable);

	public Page<Fattura> findAllByAnno(int anno, Pageable pageable);

	@Query("Select f from Fattura f where f.importo between :min and :max")
	public Page<Fattura> findByRange(BigDecimal min, BigDecimal max, Pageable pageable);

	@Query("Select f from Fattura f where f.utente.id=:id")
	public Page<Fattura> findFatturaByIdUtente(Long id, Pageable pageable);

	@Query("Select f from Fattura f where f.utente.id=:idUtente and f.numeroFattura=:numeroFattura")
	public Optional<Fattura> findFatturaByUtenteAndNumero(Long idUtente, int numeroFattura);

}
