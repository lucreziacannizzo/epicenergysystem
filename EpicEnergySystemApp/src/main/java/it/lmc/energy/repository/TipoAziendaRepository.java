package it.lmc.energy.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import it.lmc.energy.model.TipoAzienda;

public interface TipoAziendaRepository extends JpaRepository<TipoAzienda, Long> {
	public Optional<TipoAzienda> findByTipo(String tipo);
	public Optional<TipoAzienda> findById (Long id);
}
