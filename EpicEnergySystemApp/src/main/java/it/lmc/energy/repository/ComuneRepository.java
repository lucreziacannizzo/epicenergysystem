package it.lmc.energy.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import it.lmc.energy.model.Comune;
/**
 * interfaccia che mappa le chiamate di tipo CRUD sulla tabella comune del db
 * @author User
 *
 */
public interface ComuneRepository extends JpaRepository<Comune, Long> {
	public Optional<Comune> findByNome(String nome);
	public Page<Comune> findAll(Pageable pageable);
}
