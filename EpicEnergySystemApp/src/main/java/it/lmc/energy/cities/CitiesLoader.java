package it.lmc.energy.cities;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.lmc.energy.model.Comune;
import it.lmc.energy.model.Provincia;
import it.lmc.energy.service.ComuneService;
/**
 * 
 * @author Lucrezia Cannizzo(lmc)
 *
 *
 * Classe dedicata al caricamento dei comuni sul database
 */

public class CitiesLoader {
	@Autowired
	ComuneService comune;
	/**
	 * Costruisce la lista di comuni leggendo dal file csv
	 * 
	 * @return Lista di comuni
	 */
	public static List<Comune> load(InputStream is) {
		//Inizializzo la lista
		var comuni= new ArrayList<Comune>();
		//Try with resources, utilizzabile con lo scanner poichè è autocloseable
		try(var scanner= new Scanner(is)){
			//scarta le prime 3 righe di intestazione del file
			for(var i=0; i<3; i++) {
				scanner.nextLine();
			}
			
			while(scanner.hasNextLine()) {
				//Ciascuna linea del file viene salavata in una stringa
				String line= scanner.nextLine();
				//La stringa viene divisa in un array di stringhe sulla base del carattere indicato
				String [] parts =line.split(";");
				//seleziono dal file le informazioni che mi servono per popolare il db
				String nomeComune= parts[5];
				String nomeProvincia= parts[11];
				String acronimoProvincia= parts[14];
				
				//costruisco l'istanza di comune e la inserisco nell'arraylist
				var comune= new Comune().builder()
							.withNome(nomeComune)
							.withProvincia(new Provincia().builder()
									.withNome(nomeProvincia)
									.withSigla(acronimoProvincia)
									.build())
							.build();
				comuni.add(comune);
			}
			
			return comuni;
		}catch(Exception e) {
			throw new RuntimeException();
		}
		
	}
}
