package it.lmc.energy.controller;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.lmc.energy.model.Fattura;
import it.lmc.energy.model.Utente;
import it.lmc.energy.model.utility.ContattoUtility;
import it.lmc.energy.model.utility.EmailModel;
import it.lmc.energy.model.utility.IndirizzoUtility;
import it.lmc.energy.model.utility.PIvaModel;
import it.lmc.energy.model.utility.UtenteUtility;
import it.lmc.energy.model.utility.SaveUtenteUtility;
import it.lmc.energy.model.utility.UpdateContattoUtility;
import it.lmc.energy.service.UtenteService;

/**
 * 
 * @author User
 *Controller che gestisce le chiamate di tipo rest ricevute dal client e collega tali chiamate con i metodi che gestiscono il db di utenti
 */
@RestController
public class UtenteController {
	@Autowired
	UtenteService utenteService;

	@PreAuthorize("hasRole('USER')")
	@GetMapping("/fatturadautente")
	public ResponseEntity<?> findFatturaByIdUtente(@RequestParam Long id, Pageable pageable) {
		Page<Fattura> fatture = utenteService.findFatturaByIdUtente(id, pageable);
		if (fatture.hasContent()) {
			return new ResponseEntity<>(fatture, HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);

	}

	@PreAuthorize("hasRole('USER')")
	@GetMapping("/listautenti")
	public ResponseEntity<?> findAllSorted(@RequestParam(defaultValue = "2") int page,
			@RequestParam(defaultValue = "10") int size, @RequestParam(defaultValue = "id") String sort) {
		Page<Utente> utenti = utenteService.findAllSorted(page, size, sort);
		if (utenti.hasContent()) {
			return new ResponseEntity<>(utenti, HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
	}

	@PreAuthorize("hasRole('USER')")
	@GetMapping("/utenteid/{id}")
	public ResponseEntity<Utente> findById(@PathVariable Long id) {
		var u = utenteService.findById(id);
		if (u.isPresent()) {
			return new ResponseEntity<>(u.get(), HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/modificacontatto")
	public ResponseEntity<?> updateContatto(@RequestBody UpdateContattoUtility contatto) {
		var c = utenteService.updateContatto(contatto.getId(), contatto.getContatto());
		if (c.equals(null)) {
			return new ResponseEntity<>(c, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(c, HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/cancellautente/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		return new ResponseEntity<>(utenteService.delete(id), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/cancellautenteemail")
	public ResponseEntity<?> delete(@RequestBody EmailModel email) {
		return new ResponseEntity<>(utenteService.delete(email.getEmail()), HttpStatus.OK);

	}
	
	/**
	 * 
	 * @param email
	 * @return Utente
	 * 
	 * Metodo che cerca l'utente tramite indirizzo email. è un metodo Post perchè non vuole esporre dati sensibili nell'url
	 */
	
	@PreAuthorize("hasRole('USER')")
	@PostMapping("/utentedaemail")
	public ResponseEntity<Utente> findByEmail(@RequestBody EmailModel email){
		var u=utenteService.findByEmail(email.getEmail());
		if(u.isPresent()) {
			return new ResponseEntity<>(u.get(), HttpStatus.OK);
		}
	
		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
	}
	
	@PreAuthorize("hasRole('USER')")
	@PostMapping("/utentedapiva")
	public ResponseEntity<Utente> findByPiva(@RequestBody PIvaModel iva){
		var u= utenteService.findByPIva(iva.getPIva());
		if(u.isPresent()) {
			return new ResponseEntity<>(u.get(), HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/salva")
	public ResponseEntity<?> save(@RequestBody SaveUtenteUtility utility){
		return new ResponseEntity<>(utenteService.save(utility.getUtente(), utility.getTipo(), utility.getContatto(), utility.getIndirizzoLegale(), utility.getIndirizzoOperativo()), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('USER')")
	@GetMapping("/fatturatoannuale")
	public ResponseEntity<?> fatturatoAnnuale(@RequestParam Long id, @RequestParam int anno){
		return new ResponseEntity<>(utenteService.getFatturatoAnnualeByUtente(id, anno), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('USER')")
	@GetMapping("/sortbyfatturatoannuale")
	public ResponseEntity<?> sortByFatturatoAnnuale(@RequestParam int anno, Pageable pageable){
		return new ResponseEntity<>(utenteService.findAllSortedByFatturatoAnnuale(anno, pageable), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('USER')")
	@GetMapping("/fatturatomaggiore")
	public ResponseEntity<?> getByFatturatoMaggiore(@RequestParam BigDecimal cifra, @RequestParam int anno, Pageable pageable){
		return new ResponseEntity<>(utenteService.findByFatturatoMaggiore(cifra, anno, pageable), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('USER')")
	@GetMapping("/fatturatominore")
	public ResponseEntity<?> getByFatturatoMinore(@RequestParam BigDecimal cifra, @RequestParam int anno, Pageable pageable){
		return new ResponseEntity<>(utenteService.findByFatturatoMinore(cifra, anno, pageable), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('USER')")
	@GetMapping("/utentenome")
	public ResponseEntity<?> getByParteNome(@RequestParam String nome, Pageable pageable){
		return new ResponseEntity<>(utenteService.findByParteNome(nome, pageable), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('USER')")
	@GetMapping("/tipoazienda")
	public ResponseEntity<?> findByTipoAzienda(@RequestParam String tipo, Pageable pageable){
		return new ResponseEntity<>(utenteService.findAllByTipoAzienda(tipo, pageable), HttpStatus.OK);
	}
}
