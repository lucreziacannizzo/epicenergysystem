package it.lmc.energy.controller;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.lmc.energy.model.Fattura;
import it.lmc.energy.model.utility.FatturaSaveUtility;
import it.lmc.energy.service.FatturaService;

/**
 * 
 * @author User
 *RestController che collega le chiamate di tipo Rest con i metodi creati per gestire il db delle fatture
 */
@RestController
@RequestMapping("/fattura")
public class FatturaController {
	@Autowired
	FatturaService fatturaService;
	Logger log= LoggerFactory.getLogger(FatturaController.class);
	
	@DeleteMapping("/delete/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	ResponseEntity<?> deleteFatturaById(@PathVariable Long id){
		return new ResponseEntity<>(fatturaService.delete(id),HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/deletefromutente/{idUtente}/{numeroFattura}")
	ResponseEntity<?> deleteFatturaFromUtente(@PathVariable Long idUtente, @PathVariable int numeroFattura){
		return new ResponseEntity<>(fatturaService.delete(idUtente, numeroFattura), HttpStatus.OK);
	}
	@PreAuthorize("hasRole('USER')")
	@GetMapping("/fatturautente")
	ResponseEntity<?> findFatturaByUtente(@RequestParam Long idUtente, Pageable pageable){
		Page<Fattura> fatture= fatturaService.findFatturaByUtente(idUtente, pageable);
		if(fatture.hasContent()) {
			return new ResponseEntity<>(fatture, HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
	}
	
	@PreAuthorize("hasRole('USER')")
	@GetMapping("/fatturastato/{stato}")
	ResponseEntity<?> findByStato(@PathVariable String stato, Pageable pageable){
		Page<Fattura> fatture= fatturaService.findByStato(stato, pageable);
		if(fatture.hasContent()) {
			return new ResponseEntity<>(fatture, HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
	}
	
	@PreAuthorize("hasRole('USER')")
	@GetMapping("/fatturadata")
	ResponseEntity<?> findByData(@RequestParam int g, @RequestParam int m, @RequestParam int a,  Pageable pageable){
		Page<Fattura> fatture= fatturaService.findByData(g, m, a, pageable);
		if(fatture.hasContent()) {
			return new ResponseEntity<>(fatture, HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
	}
	
	@PreAuthorize("hasRole('USER')")
	@GetMapping("/fatturaanno")
	ResponseEntity<?> findByAnno(@RequestParam int anno, Pageable pageable){
		Page<Fattura> fatture=fatturaService.findByAnno(anno, pageable);
		if(fatture.hasContent()) {
			return new ResponseEntity<>(fatture, HttpStatus.OK);
		}
		log.error("Nessuna fattura trovata per l'anno scelto");
		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
	}
	
	@PreAuthorize("hasRole('USER')")
	@GetMapping("/fatturarange/{min}/{max}")
	ResponseEntity<?> findByRange(@PathVariable BigDecimal min, @PathVariable BigDecimal max, Pageable pageable){
		Page<Fattura> fatture= fatturaService.findByRange(min, max, pageable);
		if(fatture.hasContent()) {
			return new ResponseEntity<>(fatture, HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/fatturasave")
	ResponseEntity<?> save(@RequestBody FatturaSaveUtility fUtility){
		Fattura fattura= fatturaService.save(fUtility.getIdUtente(), fUtility.getImporto());
		if(fattura.equals(null)) {
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(fattura, HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/fatturamodificastato")
	ResponseEntity<String> updateStato(@RequestParam Long idFattura, @RequestParam String stato){
		return new ResponseEntity<>(fatturaService.updateStato(idFattura, stato), HttpStatus.OK);
	}
}
