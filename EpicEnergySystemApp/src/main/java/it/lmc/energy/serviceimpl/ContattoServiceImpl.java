package it.lmc.energy.serviceimpl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.lmc.energy.model.Contatto;
import it.lmc.energy.model.utility.ContattoUtility;
import it.lmc.energy.repository.ContattoRepository;
import it.lmc.energy.service.ContattoService;
@Service
public class ContattoServiceImpl implements ContattoService {
	@Autowired
	ContattoRepository contattoRepository;

	@Override
	public Optional<Contatto> findByEmail(String Email) {
		try {
			return contattoRepository.findByEmail(Email);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Optional<Contatto> findByTelefono(String telefono) {
		try {
			return contattoRepository.findByTelefono(telefono);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Page<Contatto> findAll(Pageable pageable) {
		try {
			return contattoRepository.findAll(pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Page<Contatto> findByNomeECognome(String nome, String cognome, Pageable pageable) {
		try {
			return contattoRepository.findAllByNomeAndCognome(nome, cognome, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	/**
	 * Salva un nuovo contatto nel db assicurandosi che l'email resti una chiave univoca. Se l'email esiste già nel db, ritorna l'entry associata a quella email. In ingresso accetta la classe utility che mappa la classe persistente Contatto
	 */
	@Override
	public Contatto save(ContattoUtility contatto) {
		try {
			if (contattoRepository.findByEmail(contatto.getEmail()).isEmpty()) {
				Contatto c = Contatto.builder()
						.withEmail(contatto.getEmail())
						.withCognome(contatto.getCognome())
						.withNome(contatto.getNome())
						.withTelefono(contatto.getTelefono())
						.build();
				return contattoRepository.save(c);
			}

			return contattoRepository.findByEmail(contatto.getEmail()).get();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public String delete(Long id) {
		try {
			var c= contattoRepository.findById(id);
			if(c.isPresent()) {
				contattoRepository.delete(c.get());
				return "Contatto "+c.get().getNome()+" "+c.get().getCognome()+" è stato eliminato correttamente";
			} 
			return "Il contatto non è stato eliminato";
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Contatto save(Contatto contatto) {
		try {
			if (contattoRepository.findByEmail(contatto.getEmail()).isEmpty()) {
				return contattoRepository.save(contatto);
			}
			return contattoRepository.findByEmail(contatto.getEmail()).get();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Optional<Contatto> findById(Long id) {
		try {
			return contattoRepository.findById(id);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

}
