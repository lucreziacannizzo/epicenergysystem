package it.lmc.energy.serviceimpl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.lmc.energy.model.Provincia;
import it.lmc.energy.repository.ProvinciaRepository;
import it.lmc.energy.service.ProvinciaService;

@Service
public class ProvinciaServiceImpl implements ProvinciaService {
	@Autowired
	ProvinciaRepository provinciaRepository;
	@Override
	public Provincia save(Provincia provincia) {
		try {
			return provinciaRepository.save(provincia);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		
	}
	@Override
	public Optional<Provincia> findBySigla(String sigla) {
		try {
			return provinciaRepository.findBySigla(sigla);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

}
