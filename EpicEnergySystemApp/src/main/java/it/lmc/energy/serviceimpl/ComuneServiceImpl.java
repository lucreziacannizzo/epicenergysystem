package it.lmc.energy.serviceimpl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.lmc.energy.cities.CitiesLoader;
import it.lmc.energy.model.Comune;
import it.lmc.energy.model.Provincia;
import it.lmc.energy.repository.ComuneRepository;
import it.lmc.energy.repository.ProvinciaRepository;
import it.lmc.energy.service.ComuneService;
import it.lmc.energy.service.ProvinciaService;

@Service
public class ComuneServiceImpl implements ComuneService {
	@Autowired
	ComuneRepository comuneRepository;
	@Autowired
	ProvinciaService provinciaService;

	@Override
	public void save(Comune comune) {
		comuneRepository.save(comune);

	}
	
	/**
	 * Carica le righe del file e popola il db. Se la provincia non è presenta la aggiunge, altrimenti aggiunge solo il comune
	 */
	@Override
	@Transactional
	public String popola() throws IOException {
		try {
			var fileName = "C:\\Users\\User\\git\\repository\\EpicEnergySystemApp\\Elenco-comuni-italiani.csv";
			var cities = CitiesLoader.load(new FileInputStream(fileName));
			Provincia provincia;

			for (var c : cities) {
				c.setNome(c.getNome().toLowerCase());
				var p = provinciaService.findBySigla(c.getProvincia().getSigla());
				if (p.isEmpty()) {
					provincia = provinciaService.save(c.getProvincia());

				} else {
					provincia = p.get();

				}
				c.setProvincia(provincia);
				comuneRepository.save(c);
			} return "Il database è stato popolato correttamente";
		} catch (Exception e) {
			return e.getMessage();
		}
		
	}

	@Override
	public Optional<Comune> findByNome(String nome) {
		try {
			nome=nome.toLowerCase();
			return comuneRepository.findByNome(nome);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
			}
	}

}
