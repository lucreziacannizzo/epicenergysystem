package it.lmc.energy.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.lmc.energy.model.Comune;
import it.lmc.energy.model.Indirizzo;
import it.lmc.energy.model.utility.IndirizzoUtility;
import it.lmc.energy.repository.IndirizzoRepository;
import it.lmc.energy.service.ComuneService;
import it.lmc.energy.service.IndirizzoService;

@Service
public class IndirizzoServiceImpl implements IndirizzoService {
	@Autowired
	IndirizzoRepository indirizzoRepository;
	@Autowired
	ComuneService comuneService;

	@Override
	public Indirizzo save(Indirizzo indirizzo) {
		try {
			if (trovaComune(indirizzo.getComune().getNome()).equals(null)) {
				return null;
			} else {
				indirizzo.setComune(trovaComune(indirizzo.getComune().getNome()));
				return indirizzoRepository.save(indirizzo);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Comune trovaComune(String comune) {
		try {
			var c = comuneService.findByNome(comune);
			if (c.isPresent()) {
				return c.get();
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}
/**
 * Salva l'indirizzo inserito. Se il comune non è presente all'interno del db, ritorna un valore nullo
 */
	@Override
	public Indirizzo save(IndirizzoUtility indirizzo) {
		try {
			if (trovaComune(indirizzo.getComune()).equals(null)) {
				return null;
			} else {
				Indirizzo i = Indirizzo.builder()
						.withVia(indirizzo.getVia())
						.withCivico(indirizzo.getCivico())
						.withLocalita(indirizzo.getLocalità())
						.withCap(indirizzo.getCap())
						.build();
				i.setComune(trovaComune(indirizzo.getComune()));
				return indirizzoRepository.save(i);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}

	}

	@Override
	public Page<Indirizzo> findAllByComune(Comune comune, Pageable pageable) {
		try {
			return indirizzoRepository.findAllByComune(comune, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		
	}

	@Override
	public Indirizzo save(IndirizzoUtility indirizzo) {
		if (trovaComune(indirizzo.getComune()).equals(null)) {
			return null;
		} else {
			Indirizzo i = Indirizzo.builder()
					.withVia(indirizzo.getVia())
					.withCivico(indirizzo.getCivico())
					.withLocalita(indirizzo.getLocalità())
					.withCap(indirizzo.getCap())
					.build();
			i.setComune(trovaComune(indirizzo.getComune()));
			return indirizzoRepository.save(i);
		}

	}

}
