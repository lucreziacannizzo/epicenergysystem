package it.lmc.energy.serviceimpl;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import it.lmc.energy.model.Contatto;
import it.lmc.energy.model.Fattura;
import it.lmc.energy.model.Utente;
import it.lmc.energy.model.utility.ContattoUtility;
import it.lmc.energy.model.utility.FatturatoAnnualeUtility;
import it.lmc.energy.model.utility.IndirizzoUtility;
import it.lmc.energy.model.utility.UtenteUtility;
import it.lmc.energy.repository.FatturaRepository;
import it.lmc.energy.repository.TipoAziendaRepository;
import it.lmc.energy.repository.UtenteRepository;
import it.lmc.energy.service.ComuneService;
import it.lmc.energy.service.ContattoService;
import it.lmc.energy.service.IndirizzoService;
import it.lmc.energy.service.ProvinciaService;
import it.lmc.energy.service.UtenteService;

@Service
public class UtenteServiceImpl implements UtenteService {
	@Autowired
	UtenteRepository utenteRepository;
	@Autowired
	FatturaRepository fatturaRepository;
	@Autowired
	ContattoService contattoService;
	@Autowired
	TipoAziendaRepository tipoRepository;
	@Autowired
	IndirizzoService indirizzoService;
	@Autowired
	ComuneService comuneService;
	@Autowired
	ProvinciaService provinciaService;
	Logger log = LoggerFactory.getLogger(UtenteServiceImpl.class);

	/**
	 * conta il totale delle fatture emesse a nome dell'utente.
	 */
	@Override
	public BigDecimal contaImportoTotale(List<Fattura> fatture) {
		BigDecimal somma = fatture.stream().map(f -> f.getImporto()).reduce(BigDecimal::add).get();
		return somma;
	}

	/**
	 * è possibile ordinare le entries del db tramite un parametro di sort scelto da
	 * client. (Tramite menu a tendina nel front end)
	 */
	@Override
	public Page<Utente> findAllSorted(Integer page, Integer size, String sort) {
		try {
			String[] attributi = { "id", "ragioneSociale", "dataInserimento", "dataUltimoContatto" };
			if (Arrays.stream(attributi).anyMatch(sort::equals)) {
				Pageable pageable = PageRequest.of(page, size, Sort.by(sort));
				Page<Utente> pageResult = utenteRepository.findAll(pageable);
				if (pageResult.hasContent()) {
					return pageResult;
				} else
					return null;
			}
			log.error("Nessuna corrispondenza sort trovata");
			return null;
		} catch (Exception e) {
			throw new RuntimeException();
		}
	}

	@Override
	public Page<Fattura> findFatturaByIdUtente(Long id, Pageable pageable) {
		try {
			return fatturaRepository.findFatturaByIdUtente(id, pageable);
		} catch (Exception e) {

			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Optional<Utente> findById(Long id) {
		try {
			return utenteRepository.findById(id);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	/**
	 * Il metodo permette di salvare un nuovo utente garantendo l'unicità della Pec,
	 * della partita Iva e dell'email aziendale. Questo ci permette di avere altre
	 * chiavi univoche note anche all'utente oltre all'id (che è noto solo in fase
	 * operativa)
	 */
	@Override
	public Utente save(UtenteUtility utente, String tipo, ContattoUtility contatto, IndirizzoUtility indirizzoLegale,
			IndirizzoUtility indirizzoOperativo) {
		try {
			if (utenteRepository.findByPec(utente.getPec()).isEmpty()
					&& utenteRepository.findByIva(utente.getPIva()).isEmpty()
					&& utenteRepository.findByEmail(utente.getEmail()).isEmpty()) {
				var u = Utente.builder().withRagioneSociale(utente.getRagioneSociale()).withIva(utente.getPIva())
						.withEmail(utente.getEmail()).withPec(utente.getPec()).withTelefono(utente.getTelefono())
						.build();
				u.setContatto(contattoService.save(contatto));
				tipo = tipo.toUpperCase();
				var t = tipoRepository.findByTipo(tipo);
				if (t.isPresent()) {
					u.setTipo(t.get());
				}
				var i1 = indirizzoService.save(indirizzoLegale);
				var i2 = indirizzoService.save(indirizzoOperativo);
				u.setSedeLegale(i1);
				u.setSedeOperativa(i2);
				u.setDataInserimento();
				u.setFatturatoAnnuale(
						fatturaRepository.fatturatoAnnuale(Calendar.getInstance().get(Calendar.YEAR), u.getId()));
				return utenteRepository.save(u);
			}
			if (utenteRepository.findByPec(utente.getPec()).isPresent()) {
				log.info("Pec già presente");
				return utenteRepository.findByPec(utente.getPec()).get();
			}
			if (utenteRepository.findByIva(utente.getPIva()).isPresent()) {
				log.info("Partita iva già presente");
				return utenteRepository.findByIva(utente.getPIva()).get();
			}
			if (utenteRepository.findByEmail(utente.getEmail()).isPresent()) {
				log.info("Email già presente");
				return utenteRepository.findByEmail(utente.getEmail()).get();
			}
			return null;
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}

	}

	/**
	 * Il metodo permette di modificare solo i campi che devono essere modificati,
	 * lasciando immutati quelli che da front end arrivano vuoti o bianchi
	 */
	@Override
	public Contatto updateContatto(Long id, ContattoUtility contatto) {
		try {
			var c = utenteRepository.findById(id);
			if (c.isPresent()) {
				var con = c.get().getContatto();
				if (!contatto.getEmail().isBlank()) {
					con.setEmail(contatto.getEmail());
				}
				if (!contatto.getNome().isBlank()) {
					con.setNome(contatto.getNome());
				}
				if (!contatto.getCognome().isBlank()) {
					con.setCognome(contatto.getCognome());
				}
				if (!contatto.getTelefono().isBlank()) {
					con.setTelefono(contatto.getTelefono());
				}
				return contattoService.save(con);
			}
			log.warn("Contatto non trovato, impossibile aggiornare!");
			return null;
		} catch (Exception e) {

			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public String delete(Long id) {
		try {
			var u = utenteRepository.findById(id);
			if (u.isPresent()) {
				utenteRepository.delete(u.get());
				return "Utente eliminato correttamente";
			} else {
				return "Utente non trovato";

			}
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	/**
	 * l'email è stata salvata in modo tale da essere una chiave univoca. è
	 * possibile quindi utilizzarla per definire l'entry da cancellare
	 */
	@Override
	public String delete(String email) {
		try {
			var u = utenteRepository.findByEmail(email);
			if (u.isPresent()) {
				utenteRepository.delete(u.get());
				return "Utente cancellato correttamente";
			} else
				return "Utente non trovato";
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}

	}

	@Override
	public Optional<Utente> findByEmail(String email) {
		try {
			return utenteRepository.findByEmail(email);
		} catch (Exception e) {

			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Optional<Utente> findByPIva(String pIva) {
		try {
			return utenteRepository.findByIva(pIva);
		} catch (Exception e) {

			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Page<Utente> findByRagioneSociale(String ragioneSociale, Pageable pageable) {
		try {
			return utenteRepository.findAllByRagioneSociale(ragioneSociale, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Page<Utente> findByDataInserimento(int d, int m, int y, Pageable pageable) {
		try {
			Calendar cal = Calendar.getInstance();
			cal.set(y, m, d);
			Date data = cal.getTime();

			return utenteRepository.findAllByDataInserimento(data, pageable);
		} catch (Exception e) {

			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Page<Utente> findByDataUltimoContatto(int d, int m, int y, Pageable pageable) {
		try {
			Calendar cal = Calendar.getInstance();
			cal.set(y, m, d);
			Date data = cal.getTime();
			return utenteRepository.findAllByDataUltimoContatto(data, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Optional<Utente> findByPec(String pec) {
		try {
			return utenteRepository.findByPec(pec);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Optional<Utente> findByTelefono(String telefono) {
		try {
			return utenteRepository.findByTelefono(telefono);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Optional<Utente> findByContattoEmail(String nome) {
		try {
			return utenteRepository.findByEmail(nome);
		} catch (Exception e) {

			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Optional<Utente> findByContattoNome(String nome, String cognome) {
		try {
			Pageable pageable = PageRequest.ofSize(3);
			Page<Contatto> contatti = contattoService.findByNomeECognome(nome, cognome, pageable);
			if (contatti.hasContent()) {
				List<Contatto> contattiList = contatti.getContent();
				if (contattiList.size() != 1) {
					log.error("I parametri non permettono di stabilire un utente univoco");
					return null;
				} else {
					return utenteRepository.findByContatto(contattiList.get(0));
					/*
					 * for (Contatto c : contattiList) { Optional<Utente> u =
					 * utenteRepository.findByContatto(c); return u; }
					 */
				}

			}
			log.error("Nessuna corrispondenza trovata per la coppia nome " + nome + " cognome " + cognome);
			return null;

		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Optional<Utente> updateDataUltimoContatto(Long id) {
		try {
			var u = utenteRepository.findById(id);
			if (u.isPresent()) {
				u.get().setDataUltimoContatto(new Date());
				utenteRepository.save(u.get());
				return u;
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public BigDecimal getFatturatoAnnualeByUtente(Long id, int anno) {
		try {
			return fatturaRepository.fatturatoAnnuale(anno, id);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Page<FatturatoAnnualeUtility> findAllSortedByFatturatoAnnuale(int anno, Pageable pageable) {
		try {
			return fatturaRepository.findAllSortedByFatturato(anno, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Page<FatturatoAnnualeUtility> findByFatturatoMinore(BigDecimal cifra, int anno, Pageable pageable) {
		try {
			return fatturaRepository.findByFatturatoMinore(cifra, anno, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Page<FatturatoAnnualeUtility> findByFatturatoMaggiore(BigDecimal cifra, int anno, Pageable pageable) {
		try {
			return fatturaRepository.findByFatturatoMaggiore(cifra, anno, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Optional<Utente> findByContattoTelefono(String telefono) {
		try {
			var c = contattoService.findByTelefono(telefono);
			if (c.isPresent()) {
				var u = utenteRepository.findByContatto(c.get());
				if (u.isPresent()) {
					return u;
				}
				log.warn("Nessun Utente legato al contatto");
			}
			log.error("Nessun contatto legato al numero di telefono cercato");
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Page<Utente> findByParteNome(String nome, Pageable pageable) {
		try {
			Page<Utente> utenti = utenteRepository.findByParteNome(nome, pageable);
			if (utenti.hasContent()) {
				return utenti;
			}
			log.error("Nessun utente trovato");
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Page<Utente> findAllByTipoAzienda(String nome, Pageable pageable) {
		try {
			var t = tipoRepository.findByTipo(nome);
			if (t.isPresent()) {
				return utenteRepository.findAllByTipo(t.get(), pageable);
			}
			log.error("Tipo azienda non trovato");
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Optional<Utente> updateFatturatoAnnuale(Long id, BigDecimal importo) {
		try {
			var u = utenteRepository.findById(id);
			if (u.isPresent()) {
				u.get().setFatturatoAnnuale(fatturaRepository
						.fatturatoAnnuale(Calendar.getInstance().get(Calendar.YEAR), u.get().getId()).add(importo));
				utenteRepository.save(u.get());
			}
			return u;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	/**
	 * Il metodo permette di salvare un nuovo utente garantendo l'unicità della Pec,
	 * della partita Iva e dell'email aziendale. Questo ci permette di avere altre
	 * chiavi univoche note anche all'utente oltre all'id (che è noto solo in fase
	 * operativa)
	 */
	@Override
	public Utente save(UtenteUtility utente, String tipo, ContattoUtility contatto, IndirizzoUtility indirizzoLegale,
			IndirizzoUtility indirizzoOperativo) {
		try {
			if (utenteRepository.findByPec(utente.getPec()).isEmpty()
					&& utenteRepository.findBypIva(utente.getPIva()).isEmpty()
					&& utenteRepository.findByEmail(utente.getEmail()).isEmpty()) {
				var u = Utente.builder().withRagioneSociale(utente.getRagioneSociale()).withPIva(utente.getPIva())
						.withEmail(utente.getEmail()).withPec(utente.getPec()).withTelefono(utente.getTelefono())
						.build();
				u.setContatto(contattoService.save(contatto));
				tipo = tipo.toUpperCase();
				var t = tipoRepository.findByTipo(tipo);
				if (t.isPresent()) {
					u.setTipo(t.get());
				}
				var i1 = indirizzoService.save(indirizzoLegale);
				var i2 = indirizzoService.save(indirizzoOperativo);
				u.setSedeLegale(i1);
				u.setSedeOperativa(i2);
				u.setDataInserimento();
				return utenteRepository.save(u);
			}
			if (utenteRepository.findByPec(utente.getPec()).isPresent()) {
				log.info("Pec già presente");				
				return utenteRepository.findByPec(utente.getPec()).get();
			}
			if (utenteRepository.findBypIva(utente.getPIva()).isPresent()) {
				log.info("Partita iva già presente");
				return utenteRepository.findBypIva(utente.getPIva()).get();
			}
			if (utenteRepository.findByEmail(utente.getEmail()).isPresent()) {
				log.info("Email già presente");
				return utenteRepository.findByEmail(utente.getEmail()).get();
			}
			return null;
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}

	}

	/**
	 * Il metodo permette di modificare solo i campi che devono essere modificati,
	 * lasciando immutati quelli che da front end arrivano vuoti o bianchi
	 */
	@Override
	public Contatto updateContatto(Long id, ContattoUtility contatto) {
		var c = contattoService.findById(id);
		if (c.isPresent()) {
			if (!contatto.getEmail().isBlank()) {
				c.get().setEmail(contatto.getEmail());
			}
			if (!contatto.getNome().isBlank()) {
				c.get().setNome(contatto.getNome());
			}
			if (!contatto.getCognome().isBlank()) {
				c.get().setCognome(contatto.getCognome());
			}
			if (!contatto.getTelefono().isBlank()) {
				c.get().setTelefono(contatto.getTelefono());
			}
			return c.get();
		}
		log.warn("Contatto non trovato, impossibile aggiornare!");
		return null;
	}

	@Override
	public String delete(Long id) {
		try {
			var u = utenteRepository.findById(id);
			if (u.isPresent()) {
				utenteRepository.delete(u.get());
				return "Utente eliminato correttamente";
			} else {
				return "Utente non trovato";

			}
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	/**
	 * Posso considerare una stringa generica usando il chain of responsibility
	 */
	@Override
	public String delete(String email) {
		try {
			var u = utenteRepository.findByEmail(email);
			if (u.isPresent()) {
				utenteRepository.delete(u.get());
				return "Utente cancellato correttamente";
			} else
				return "Utente non trovato";
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}

	}

	@Override
	public Optional<Utente> findByEmail(String email) {
		return utenteRepository.findByEmail(email);
	}

	@Override
	public Optional<Utente> findByPIva(String pIva) {
		return utenteRepository.findBypIva(pIva);
	}

	@Override
	public Page<Utente> findByRagioneSociale(String ragioneSociale, Pageable pageable) {
		return utenteRepository.findAllByRagioneSociale(ragioneSociale, pageable);
	}

	@Override
	public Page<Utente> findByDataInserimento(int d, int m, int y, Pageable pageable) {
		Calendar cal = Calendar.getInstance();
		cal.set(y, m, d);
		Date data = cal.getTime();
		;
		return utenteRepository.findAllByDataInserimento(data, pageable);
	}

	@Override
	public Page<Utente> findByDataUltimoContatto(int d, int m, int y, Pageable pageable) {
		Calendar cal = Calendar.getInstance();
		cal.set(y, m, d);
		Date data = cal.getTime();
		return utenteRepository.findAllByDataUltimoContatto(data, pageable);
	}

	@Override
	public Optional<Utente> findByPec(String pec) {

		return utenteRepository.findByPec(pec);
	}

	@Override
	public Optional<Utente> findByTelefono(String telefono) {
		return utenteRepository.findByTelefono(telefono);
	}

	@Override
	public Optional<Utente> findByContattoEmail(String nome) {
		return utenteRepository.findByEmail(nome);
	}

	@Override
	public Optional<Utente> findByContattoNome(String nome, String cognome) {
		try {
			Pageable pageable = PageRequest.ofSize(3);
			Page<Contatto> contatti = contattoService.findByNomeECognome(nome, cognome, pageable);
			if (contatti.hasContent()) {
				List<Contatto> contattiList = contatti.getContent();
				if (contattiList.size() != 1) {
					log.error("I parametri non permettono di stabilire un utente univoco");
					return null;
				} else {
					return utenteRepository.findByContatto(contattiList.get(0));
					/*
					 * for (Contatto c : contattiList) { Optional<Utente> u =
					 * utenteRepository.findByContatto(c); return u; }
					 */
				}

			}
			log.error("Nessuna corrispondenza trovata per la coppia nome " + nome + " cognome " + cognome);
			return null;

		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	}


	@Override
	public Optional<Utente> updateDataUltimoContatto(Long id) {
		try {
			var u = utenteRepository.findById(id);
			if (u.isPresent()) {
				u.get().setDataUltimoContatto(new Date());
				utenteRepository.save(u.get());
				return u;
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public BigDecimal getFatturatoAnnualeByUtente(Long id, int anno) {
		try {
			return fatturaRepository.fatturatoAnnuale(anno, id);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Page<FatturatoAnnualeUtility> findAllSortedByFatturatoAnnuale(int anno, Pageable pageable) {
		try {
			return fatturaRepository.findAllSortedByFatturato(anno, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Page<FatturatoAnnualeUtility> findByFatturatoMinore(BigDecimal cifra, int anno, Pageable pageable) {
		try {
			return fatturaRepository.findByFatturatoMinore(cifra, anno, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Page<FatturatoAnnualeUtility> findByFatturatoMaggiore(BigDecimal cifra, int anno, Pageable pageable) {
		try {
			return fatturaRepository.findByFatturatoMaggiore(cifra, anno, pageable);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Optional<Utente> findByContattoTelefono(String telefono) {
		try {
			var c = contattoService.findByTelefono(telefono);
			if (c.isPresent()) {
				var u = utenteRepository.findByContatto(c.get());
				if (u.isPresent()) {
					return u;
				}
				log.warn("Nessun Utente legato al contatto");
			}
			log.error("Nessun contatto legato al numero di telefono cercato");
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Page<Utente> findByParteNome(String nome, Pageable pageable) {
		try {
			Page<Utente> utenti = utenteRepository.findByParteNome(nome, pageable);
			if (utenti.hasContent()) {
				return utenti;
			}
			log.error("Nessun utente trovato");
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Page<Utente> findAllByTipoAzienda(String nome, Pageable pageable) {
		try {
			var t = tipoRepository.findByTipo(nome);
			if (t.isPresent()) {
				return utenteRepository.findAllByTipo(t.get(), pageable);
			}
			log.error("Tipo azienda non trovato");
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Optional<Utente> updateFatturatoAnnuale(Long id, BigDecimal importo) {
		try {
			var u = utenteRepository.findById(id);
			if (u.isPresent()) {
				u.get().setFatturatoAnnuale(fatturaRepository
						.fatturatoAnnuale(Calendar.getInstance().get(Calendar.YEAR), u.get().getId()).add(importo));
				utenteRepository.save(u.get());
			}
			return u;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}


}
