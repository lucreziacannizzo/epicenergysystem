package it.lmc.energy.serviceimpl;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.lmc.energy.model.Fattura;
import it.lmc.energy.model.utility.FatturaUtility;
import it.lmc.energy.model.utility.FatturatoAnnualeUtility;
import it.lmc.energy.repository.FatturaRepository;
import it.lmc.energy.repository.StatoFatturaRepository;
import it.lmc.energy.repository.UtenteRepository;
import it.lmc.energy.service.FatturaService;
import it.lmc.energy.service.UtenteService;

@Service
public class FatturaServiceImpl implements FatturaService {
	@Autowired
	FatturaRepository fatturaRepository;
	@Autowired
	StatoFatturaRepository statoFatturaRepository;
	@Autowired
	UtenteService utenteService;

	Logger log = LoggerFactory.getLogger(FatturaServiceImpl.class);

	@Override
	public Page<Fattura> findFatturaByUtente(Long idUtente, Pageable pageable) {
		try {
			return fatturaRepository.findFatturaByIdUtente(idUtente, pageable);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Page<Fattura> findByStato(String statoFatt, Pageable pageable) {
		try {
			var s = statoFatturaRepository.findByStato(statoFatt);
			if (s.isPresent()) {
				return fatturaRepository.findAllByStato(s.get(), pageable);
			} else
				return null;
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public Page<Fattura> findByData(int day, int month, int year, Pageable pageable) {
		try {
			Calendar cal = Calendar.getInstance();
			cal.set(year, month, day);
			Date data = cal.getTime();
			return fatturaRepository.findAllByData(data, pageable);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}

	}

	@Override
	public Page<Fattura> findByAnno(int anno, Pageable pageable) {
		try {
			return fatturaRepository.findAllByAnno(anno, pageable);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	}
/**
 * cerca le fatture emesse il cui importo è compreso tra il minimo e il massimo
 */
	@Override
	public Page<Fattura> findByRange(BigDecimal min, BigDecimal max, Pageable pageable) {
		try {
			return fatturaRepository.findByRange(min, max, pageable);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	}
/**
 * metodo che salva una nuova fattura nel db. Contemporaneamente aggiorna i campi dataUltimoContatto e FatturatoAnnuale dell'utente collegato alla fattura. Setta di default lo stato della fattura "daPagare" e il numero della fattura dipende dal numero delle fatture emesse a nome di quel cliente
 */
	@Override
	public Fattura save(Long idUtente, BigDecimal importo) {
		try {
			int precedentiFatture = 0;
			var u= utenteService.updateDataUltimoContatto(idUtente);
			//var u = utenteRepository.findById(idUtente);
			if (u.isPresent()) {
				utenteService.updateFatturatoAnnuale(u.get().getId(), importo);
				Pageable pageable = PageRequest.ofSize(5);
				Page<Fattura> fattureUtente = fatturaRepository.findFatturaByIdUtente(idUtente, pageable);
				if (fattureUtente.hasContent()) {
					List<Fattura> listaFatture = fattureUtente.getContent();
					precedentiFatture = listaFatture.size();
				}
				var f= fatturaRepository.save(Fattura.builder().withImporto(importo).withUtente(u.get())
						.withNumeroFattura(precedentiFatture + 1).build());
				f.setData();
				f.setAnno();
				var s=statoFatturaRepository.findByStato("daPagare");
				if(s.isPresent()) {
				f.setStato(statoFatturaRepository.findByStato("daPagare").get());
				log.error("Stato non impostato perchè non presente! aggiungerlo o correggerlo");
				}
				return fatturaRepository.save(f);
			} else {
				log.error("Id utente non presente, fattura non salvata");
				return null;
			}

		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}

	}

	@Override
	public String delete(Long idFattura) {
		try {
			var f = fatturaRepository.findById(idFattura);
			if (f.isPresent()) {
				fatturaRepository.delete(f.get());
				return "Fattura " + idFattura + " eliminata correttamente";
			} else
				return "Fattura " + idFattura + " non presente";
		} catch (Exception e) {
			return e.getMessage();
		}

	}
/**
 * elimina una specifica fattura emessa ad un determinato utente definito tramite id. Riceve in ingresso l'idUtente e il numeroFattura
 */
	@Override
	public String delete(Long idUtente, int numeroFattura) {
		try {
			var f = fatturaRepository.findFatturaByUtenteAndNumero(idUtente, numeroFattura);
			if (f.isPresent()) {
				fatturaRepository.delete(f.get());
				return "La fattura è stata cancellata correttamente";
			} else {
				return "La fattura cercata non è presente";
			}
		} catch (Exception e) {
			return e.getMessage();
		}

	}
	
	/**
	 * metodo che permette di aggiornare lo stato di una fattura, usando tra gli stati salvati in precedenza nella tabella statoFattura del db.
	 */

	@Override
	public String updateStato(Long idFattura, String stato) {
		try {
			var f = fatturaRepository.findById(idFattura);
			var s = statoFatturaRepository.findByStato(stato);
			if (f.isPresent()) {
				if (s.isPresent()) {
					f.get().setStato(s.get());
					fatturaRepository.save(f.get());
					return "Stato fattura " + idFattura + " aggiornato in " + stato;
				}
				log.error("Stato non presente");
				return "Lo stato " + stato
						+ " non è valido. Si prega di salvarlo correttamente prima di utilizzarlo. Fattura non aggiornata";
			}
			log.error("Fattura non presente");
			return "La fattura " + idFattura + " non è presente nel database";
		} catch (Exception e) {
			return e.getMessage();
		}
	}
/**
 * metodo che aggiorna l'importo di una fattura
 */
	@Override
	public Fattura update(Long idFattura, FatturaUtility fattura) {
		try {
			var f= fatturaRepository.findById(idFattura);
			if(f.isPresent()) {
				f.get().setImporto(fattura.getImporto());
				return f.get();
			}else {
				log.error("Fattura non trovata");
				return null;
			}
		}catch(Exception e) {
			
		}
		return null;
	}

}
