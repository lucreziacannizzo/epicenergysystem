package it.lmc.energy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import it.lmc.energy.service.ComuneService;
/**
 * runner che viene lanciato per popolare le tabelle comune e provincia del db
 * @author User
 *
 */
@Component
public class Runner implements CommandLineRunner {
	@Autowired
	ComuneService comune;
	Logger log= LoggerFactory.getLogger(Runner.class);
	@Override
	public void run(String... args) throws Exception {
		//La riga viene lanciata per popolare il db dei comuni e delle province
		//log.info(comune.popola());
		
		
	}

}
