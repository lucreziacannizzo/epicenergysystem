package it.lmc.energy.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import javax.persistence.JoinColumn;

/**
 * Mappa i dettagli di login degli utenti  
 * @author User
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
public class UserLogin extends BaseEntity {
	private String username;
	private String email;
	private String password;
	private boolean active;
	private String nome;
	private String cognome;
	@ManyToMany
	@JoinTable(	name = "user_roles", joinColumns = @JoinColumn(name = "userlogin_id"), inverseJoinColumns = @JoinColumn(name = "ruolo_id"))
	private Set<Role> role;
}
