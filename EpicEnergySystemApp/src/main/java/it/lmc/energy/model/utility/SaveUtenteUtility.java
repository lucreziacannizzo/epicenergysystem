package it.lmc.energy.model.utility;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * 
 * @author User
 *classe utility che mappa valori ricevuti tramite body di una chiamata rest di tipo post o put e permette di creare un'istanza di classe utente da rendere persistente
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SaveUtenteUtility {
	UtenteUtility utente;  
	String tipo;  
	ContattoUtility contatto;  
	IndirizzoUtility indirizzoLegale;  
	IndirizzoUtility indirizzoOperativo;
}
