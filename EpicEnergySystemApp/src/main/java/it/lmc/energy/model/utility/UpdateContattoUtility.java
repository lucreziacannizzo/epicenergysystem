package it.lmc.energy.model.utility;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author User
 *Classe Utility che mappa i valori ricevuti da body di chiamata rest che permette la creazione di istanza di classe Contatto
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateContattoUtility {
	private Long id; 
	private ContattoUtility contatto;
}
