package it.lmc.energy.model.utility;

import java.math.BigDecimal;

import it.lmc.energy.model.Utente;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * 
 * @author User
 *Interfaccia che permette di mappare i valori ricevuti dal db, selezionati tramite query
 */
public interface FatturatoAnnualeUtility {
	public String getEmail();
	public BigDecimal getImporto();
}
