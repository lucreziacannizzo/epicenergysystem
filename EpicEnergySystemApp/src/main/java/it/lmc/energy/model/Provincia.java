package it.lmc.energy.model;

import javax.persistence.Column;
import javax.persistence.Entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
/**
 * 
 * @author User
 *
 */

@Entity
@Builder(setterPrefix = "with")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Provincia extends BaseEntity {
	private String nome;
	@Column(length = 2)
	private String sigla;
	
}
