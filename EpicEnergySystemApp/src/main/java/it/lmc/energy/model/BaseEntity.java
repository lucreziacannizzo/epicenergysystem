package it.lmc.energy.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import lombok.Data;
/**
 * 
 * @author User
 *Classe base. Gestisce la chiave primaria di tutte le entità
 */
@MappedSuperclass
@Data
public class BaseEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	 private Long id;
}
