package it.lmc.energy.model.utility;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author User
 *classe utility che mappa entità di tipo stringa per gestire body di chiamate rest di tipo post
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmailModel {
	private String email;
}
