package it.lmc.energy.model.utility;

import it.lmc.energy.model.TipoAzienda;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * 
 * @author User
 *classe utility utilizzata per mappare i valori ricevuti per creare istanze di tipo utente
 */
@Data
@Builder(setterPrefix = "with")
@NoArgsConstructor
@AllArgsConstructor
public class UtenteUtility {
	private String ragioneSociale;
	private String pIva;
	private String email;
	private String pec;
	private String telefono;
	private ContattoUtility contatto;
	private TipoAzienda tipo;
}
