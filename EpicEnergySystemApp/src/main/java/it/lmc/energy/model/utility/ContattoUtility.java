package it.lmc.energy.model.utility;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * 
 * @author User
 * classe utility che mappa valori ricevuti da form. Permette di maneggiare tali valori per creare istanze di tipo contatto
 */
@Data
@Builder(setterPrefix = "with")
@NoArgsConstructor
@AllArgsConstructor
public class ContattoUtility {
	 String email;
	 String nome;
	 String cognome;
	 String telefono;

}
