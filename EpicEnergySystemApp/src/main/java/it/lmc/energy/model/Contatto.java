package it.lmc.energy.model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder(setterPrefix = "with")
public class Contatto extends BaseEntity {
	private String email;
	private String nome;
	private String cognome;
	private String telefono;
	@OneToOne
	private Utente utente;
	
	
}
