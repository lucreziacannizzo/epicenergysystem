package it.lmc.energy.model.utility;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * 
 * @author User
 *classe utility che mappa i valori ricevuti da body di una chiamata rest e che permette di creare un'istanza di Indirizzo
 */
@Data
@Builder(setterPrefix = "with")
@AllArgsConstructor
@NoArgsConstructor
public class IndirizzoUtility {
	private String via;
	private int civico;
	private String località;
	private String comune;
	private Long cap;
}
