package it.lmc.energy.model;

import javax.persistence.Entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 
 * @author User
 * Classe che mi permette di salvare i possibili stati di pagamento di una fattura e mi permette di aggiungerne nuovi
 */
@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class StatoFattura extends BaseEntity {
	private String stato;
}
