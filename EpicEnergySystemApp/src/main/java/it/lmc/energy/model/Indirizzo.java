package it.lmc.energy.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@Builder(setterPrefix = "with")
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Indirizzo extends BaseEntity {
	private String via;
	private int civico;
	private String localita;
	private Long cap;
	@ManyToOne
	private Comune comune;
	
}
