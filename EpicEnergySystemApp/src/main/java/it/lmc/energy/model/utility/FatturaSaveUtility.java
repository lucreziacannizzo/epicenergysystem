package it.lmc.energy.model.utility;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author User
 *classe utility che mappa valori per permettere la creazione di un'istanza di tipo Fattura
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FatturaSaveUtility {
	private Long idUtente;
	private BigDecimal importo;
}
