package it.lmc.energy.test;


import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import it.lmc.energy.model.Fattura;
import it.lmc.energy.service.FatturaService;

@WebMvcTest
public class FatturaTest {
	
	@Autowired
	private MockMvc mvc;
	@MockBean
	private FatturaService service;
	
	Fattura test= Fattura.builder()
			.withImporto(new BigDecimal(345.00))
			.build();
	
}
